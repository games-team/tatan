module br.append;
private import opengl;
private import util.key;
private import util.shape;
private import br.mainloop;
private import util.vector;
private import util.matrix;
private import std.math;
private import br.ship;
private import br.enemy;
private import br.gamemanager;
private import util.particle;
private import util.parts;
private import util.log;
private import br.particleImpl;

public class Append:Parts{
	private:
		int vanishCount;
		bool parentExist;
	
	public this(Shape shape ,double size ,double R = 1.0,double G = 1.0 ,double B = 1.0,double alpha = 1.0 ,Matrix poseBase = new Matrix()){
		this.shape = cast(Shape)shape.clone();
		drawing = WIRE;
		this.size = size;
		setPoseBase(poseBase);
//		appendManager.add(this);
		this.R = R;
		this.G = G;
		this.B = B;
		this.alpha = alpha;
		parentExist = true;
	}
	public void setCollision(double collisionSize){
		collisionManager.add(this, collisionManager.kind.SHIP ,2);
//		collisionManager.add(this, collisionManager.kind.SWORD ,2);
		this.collisionRange = collisionSize;
		drawing = POLYGON | WIRE;
	}
	public void move(){
		super.move();
		if(parentExist & (parent is null || parent.exists == false)){
			parentExist = false;
			vanishCount = 5;
			
		}
		if(parentExist == false){
			vanishCount --;
			if(vanishCount < 0)destroy();
		}
	}
	public void destroy(){
		super.destroy();
		makeParticle(shape, size ,WIRE ,rpos ,new Vector3() ,rpose ,R ,G ,B ,wR,wG,wB,alpha);
	}
	
}

/+
public class BaseOfWing:Parts{
	private{
		static  float[][] a = 
		[
    	    [-0.2 ,0 ,0.2   ],
					[-0.2 ,0 ,0.2 ]
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ],
				 [0 ,1.0 ,0 ]
	       
	     ];
	}
	
	public this(){
		shape = BaseOfWing1.getShape();//new SH_Pole(a,b,4);
		drawing = WIRE;
		size = 30;
		
	}
}
public class Wing:Parts{
	private{
		static  float[][] a = 
		[
    	    [-1.0 ,-0.5 ,1.0  ],
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ]
	       
	     ];
	}
	
	public this(){
		/*
		Shape shape1 = new SH_Pole(a,b,4);
		Shape shape2 = cast(SH_Pole)shape1.clone();
		shape1.translate(new Vector3(0.1 ,0 ,0));
		shape2.translate(new Vector3(-0.1 ,0 ,0));
		shape = shape1 + shape2;
		*/
		shape = Wing1.getShape();//new SH_Pole(a,b,4);
		//shape.translate(new Vector3(1.0 ,0 ,0));
		
	
		drawing = WIRE;
		size = 20;
		
	}
}
public class Tail:Parts{
	private{
		static  float[][] a = 
		[
    	    [-1.0 ,-0.5 ,1.0  ],
	
	     ];
	  static float[][] b =
		[
	       [0 ,0.5 ,0   ]
	       
	     ];
	}
	
	public this(){
		shape = Tail1.getShape();//new SH_Pole(a,b,4);
		drawing = WIRE;
		size = 20;
		
	}
}
public class Head:Parts{
	private{
		static  float[] a = 
    	    [-1.0 ,-0.5 ,1.0 ,-0.5 ]
					;
	  static float[] b =
	       [0 ,0.5 ,0 ,-0.5   ]
	       ;
		static float[] z =
					[-0.5 ,0 ,0.5];
		static float[] scale =
					[0.3 ,1.0 ,0.3];
	}
	
	public this(){
		shape = Head1.getShape();//new SH_Pot(a,b,z,scale);
		drawing = WIRE;
		size = 30;
		
	}
}
+/