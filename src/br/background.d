module br.background;

private import opengl;
private import std.math;
private import std.string;
private import util.vector;
private import util.parts;
private import br.screen;
private import br.pattern;
private import br.gamemanager;


public class BackGround{
	public:
	int cnt;
	double R,G,B;
	public this(){
		patternManager.clear();
		cnt = 0;
		set();
	}
	public abstract void set();
	public abstract void run(){
		cnt ++;
	}
	public void draw(){
		glDisable(GL_LINE_SMOOTH);
		glDisable(GL_BLEND);
		glDisable(GL_LIGHTING);
		glDisable(GL_DEPTH_TEST);
		
		glColor3f(R ,G ,B);
		glBegin(GL_QUADS);
		glVertex3f(Screen.GAME_LEFT ,Screen.GAME_UP ,-800.0f);
		glVertex3f(Screen.GAME_LEFT ,Screen.GAME_DOWN ,-800.0f);
		glVertex3f(Screen.GAME_RIGHT ,Screen.GAME_DOWN ,-800.0f);
		glVertex3f(Screen.GAME_RIGHT ,Screen.GAME_UP ,-800.0f);
		glEnd();
	}
}


public class Back1:BackGround{
	private:
	int scnt;
	public this(){
		super();
		scnt = 0;
		R=83.0f/255.0f;
		G=177.0f/255.0f;
		B=205.0f/255.0f;
	}
	public void set(){
		for(float z = -400;-1200<z;z-=200.0f){
			for(float y=-200;y<340;y+=100){
				for(float x = -200;x<=200;x += 100){//114){
					Vector3 pos = new Vector3(x ,y ,z);
					Vector3 vel = new Vector3(0.0f ,-1.0f ,0);
					new Pattern2(pos ,vel);
				}
			}
		}
	}
	
	public void run(){
		if((scnt) >= divsp(100)){
			for(float z = -400;-1200<z;z-=200.0f){
				for(float x = -200;x<=200;x += 100){//114){
					Vector3 pos = new Vector3(x ,Screen.GAME_UP+60 ,z);
					Vector3 vel = new Vector3(0.0f ,-1.0f ,0);
					new Pattern2(pos ,vel);
				}
			}
			scnt = 0;
		}
		super.run();
		scnt++;
	}
	
	
	
	
}

public class Back2:BackGround{
	private:
	int scnt;
	public this(){
		super();
		scnt = 0;
		R=83.0f/255.0f;
		G=177.0f/255.0f;
		B=205.0f/255.0f;
	}
	public void set(){
		for(float z = -400;-1200<z;z-=200.0f){
			for(float y=-200;y<340;y+=100){
				for(float x = -200;x<=200;x += 100){//114){
					Vector3 pos = new Vector3(x ,y ,z);
					Vector3 vel = new Vector3(0.0f ,-3.0f ,0);
					new Pattern2(pos ,vel);
				}
			}
		}
	}
	
	public void run(){
		if((scnt) >= divsp(33)){
			for(float z = -400;-1200<z;z-=200.0f){
				for(float x = -200;x<=200;x += 100){//114){
					Vector3 pos = new Vector3(x ,Screen.GAME_UP+60 ,z);
					Vector3 vel = new Vector3(0.0f ,-3.0f ,0);
					new Pattern2(pos ,vel);
				}
			}
			scnt = 0;
		}
		super.run();
		scnt++;
	}
	
	
	
	
}

public class Back3:BackGround{
	private:
	int scnt;
	const double tR = 233.0f/255.0f;
	const double tG = 121.0f/255.0f;
	const double tB = 146.0f/255.0f;
	public this(){
		super();
		scnt = 0;
		R=83.0f/255.0f;
		G=177.0f/255.0f;
		B=205.0f/255.0f;
	}
	public void set(){}
	public void start(){
		Parts p = new Pattern4(new Vector3(0,0,-800));
		(cast(Pattern)p).border = false;
		Parts p1;
//		double radius = 600.0f;
		Pattern3.RADIUS = 1000.0f;
		double theta ,phi;
		int num;
		for(phi=-90;phi<=91;phi+=15.0){
			num = cast(int)fmax(1,cos(phi*PI/180.0) * 24.0);
			for(theta=0.0;theta<360;theta+=(360.0/num)){
				p1 = new Pattern3();(cast(Pattern)p1).border = false;
				p.addChild(p1 ,std.string.toString(phi)~std.string.toString(theta),Pattern3.RADIUS ,Parts.ENGAGED);
				p1.linkZ = theta;
				p1.linkY = phi;
			}
		}
	}
	
	
	public void run(){
		super.run();
		if(cnt < divsp(120)){
			
		}else if(cnt < divsp(240)){
			R *= 1.0f -mulsp(0.02f);
			G *= 1.0f -mulsp(0.02f);
			B *= 1.0f -mulsp(0.02f);
		}else if(cnt == divsp(240)){
			start();
		}else {
			if(R<tR)R+= tR/divsp(260.0f);
			else R =tR;
			if(G<tG)G+=tG/divsp(260.0f);
			else G=tG;
			if(B<tB)B+=tB/divsp(260.0f);
			else B=tB;
			if(600.0f<Pattern3.RADIUS)Pattern3.RADIUS -= mulsp(2.0f);
			else Pattern3.RADIUS = 600.0;
		}
	}
	
	
	
	
}