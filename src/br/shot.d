module br.shot;

private import util.parts;
private import util.particle;
private import br.shapeImpl;
private import br.screen;
private import br.mainloop;
private import br.gamemanager;
private import util.laser;
private import br.particleImpl;

public class Shot:Parts{
	public Vector3 vel;
	public static POWER = 1.0f;
	public static SIZE = 30.0f;
	public this(Vector3 pos ,Vector3 vel ,double deg){
		collisionManager.add(this ,collisionManager.kind.SHOT ,1);
		this.pos = cast(Vector3)pos.clone();
		rpos = cast(Vector3)pos.clone();
		this.vel = cast(Vector3)vel.clone();
		
		shape = BulletShape.getShape();
		size = 0.0f;//30.0f;
		drawing =  POLYGON | WIRE;
		collisionRange = SIZE;
		R = 0.7;G =0.88;B = 1.0;alpha = 1.0;
		
		wR = 0.8;wG =0.92;wB = 1.0f;walpha = 1.0;
		poseZ = atan2(vel.y,vel.x)*180.0/PI;// + 90;
		
		this.poseX = deg;
		shotManager.add(this);
	}
	
	public void move(){
		super.move();
		if(cnt <=1)size = 0.0f;
//		else if(cnt == 3)size = SIZE * 2.0f / 3.0f;
		else size = SIZE;
		if((rpos.y+size < screen.GAME_DOWN || screen.GAME_UP < rpos.y-size ||
			screen.GAME_RIGHT  < rpos.x-size || rpos.x+size < screen.GAME_LEFT ||
			screen.GAME_NEAR < rpos.z - size || rpos.z + size < screen.GAME_FAR
			
		)){
			vanish();
		}
		pos += vel;
		
		
		poseX += 20.0f;
	}
	public override void reportCollision(int kind ,Parts p){
		destroy();
	}
	public void destroy(){
		super.destroy();
		makeSimpleParticle(2, size/5.0f ,WIRE ,rpos ,R ,G ,B ,wR,wG,wB,alpha);

	}
}