module br.wall;

private import util.parts;
private import util.particle;
private import util.collision;
private import br.shapeImpl;
private import br.screen;
private import br.mainloop;
private import br.gamemanager;

public void makeWall(char[] type ,Vector3 pos ,Vector3 vel ,Object par = null){
	switch(type){
		case "1" : new Wall1(pos ,vel);
		
		default:break;
	}
}

public class Wall:Parts{

//	int hp;
	public this(Vector3 pos ,float size=30.0f){
		collisionManager.add(this ,collisionManager.kind.SHIP ,2);
		collisionManager.add(this ,collisionManager.kind.SHOT ,2);
		collisionManager.add(this ,collisionManager.kind.WALL ,1);
		this.pos = cast(Vector3)pos.clone();
		rpos = cast(Vector3)pos.clone();
		
		drawing =   POLYGON | WIRE;
		
		wR = 0.0;wG =0.0f;wB = 0.0f;walpha = 1.0;
		
		this.size = size;
		collisionRange = size / 2.0f;
		
		wallManager.add(this);
		
//		hp = 3;
	}
	
	public void move(){
		super.move();
		if((rpos.y+size < screen.GAME_DOWN || screen.GAME_UP < rpos.y-size ||
			screen.GAME_RIGHT  < rpos.x-size || rpos.x+size < screen.GAME_LEFT ||
			screen.GAME_NEAR < rpos.z - size || rpos.z + size < screen.GAME_FAR
			
		)){
			vanish();
		}
		
	}
	public override void reportCollision(int kind ,Parts p){
		switch(kind){
//			case CollisionManager.kind.SHIP:
//			destroy();
//			break;
			default:break;
		}
		
	}
	public void destroy(){
		super.destroy();
//		makeParticle(shape, size ,WIRE ,rpos ,new Vector3() ,rpose ,R ,G ,B ,alpha);

	}
	
}

public class Wall1:Wall{
	public:
	Vector3 vel;
	
	public this(Vector3 pos ,Vector3 vel){
		super(pos ,30.0f);
		this.vel = cast(Vector3)vel.clone();
		
		shape = Cube.getShape();
		
		
		R = 0.88;G =1.0;B = 0.9;alpha = 1.0;
		
		poseZ = atan2(vel.y,vel.x)*180.0/PI;
	}
	public void move(){
		super.move();
		
		pos += mulsp(vel);
		
		poseX += mulsp(2f);
	}
	
}