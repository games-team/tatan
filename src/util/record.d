module util.record;
private import std.stream;

public class Record{
	
	public:
	int[][] lapTime;
	int[][] totalTime;
	
	public this(int stage ,int rank){
		lapTime.length = stage;
		totalTime.length = stage;
		foreach(inout int[] lap;lapTime){
			lap.length = rank;
			
		}
		foreach(inout int[] total;totalTime){
			total.length = rank;
			
		}
		start();
	}
	public void start(){
		foreach(inout int[] lap;lapTime){
			foreach(inout int l;lap){
				l = 359999;
			}
		}
		foreach(inout int[] total;totalTime){
			foreach(inout int t;total){
				t = 359999;
			}
		}
	}
	public void load(File fd){
		foreach(inout int[] lap;lapTime){
			foreach(inout int l;lap){
				fd.read(l);
			}
		}
		foreach(inout int[] total;totalTime){
			foreach(inout int t;total){
				fd.read(t);
			}
		}
	}
	public void save(File fd){
		foreach(inout int[] lap;lapTime){
			foreach(inout int l;lap){
				fd.write(l);
			}
		}
		foreach(inout int[] total;totalTime){
			foreach(inout int t;total){
				fd.write(t);
			}
		}
	}
	public int updateLapRecord(int stage ,int lap){
		int rank = -1;
		if(lapTime.length <= stage)return -1;
		for(int i=0;i<lapTime[stage].length;i++){
			if(lap < lapTime[stage][i]){
				rank = i+1;
				for(int j=lapTime[stage].length-1;i<j;j--){
					lapTime[stage][j] = lapTime[stage][j-1];
				}
				lapTime[stage][i] = lap;
				return rank;
			}
			
		}
		return rank;
	}
	public int updateTotalRecord(int stage ,int total){
		int rank = -1;
		if(totalTime.length <= stage)return -1;
		for(int i=0;i<totalTime[stage].length;i++){
			if(total < totalTime[stage][i]){
				rank = i+1;
				for(int j=totalTime[stage].length-1;i<j;j--){
					totalTime[stage][j] = totalTime[stage][j-1];
				}
				totalTime[stage][i] = total;
				return rank;
			}
			
		}
		return rank;
	}
}
