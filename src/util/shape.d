module util.shape;
private import std.math;
private import util.cloneable;
private import std.string;
private import util.vector;
private import util.matrix;
private import util.log;
/*
public struct polyRect{
	public int[4] v;
	public Vector3 normal;
	//public Vector3[4] v;
	
	//public Vector3 normal;
}
*/


/*
public Vector3 getNormal(Vector3[4] v){
	Vector3 v0,v1,v2;
	if((1e-6)<=v[0].dist(v[1])){
		if((1e-6)<=v[0].dist(v[2])){
			v0=v[0];v1=v[1];v2=v[2];
		}else if((1e-6)<=v[0].dist(v[3])){
			v0=v[0];v1=v[1];v2=v[3];
		}else return null;
	}else{
		if((1e-6)<=v[1].dist(v[2]) && (1e-6)<=v[1].dist(v[3])){
			v0=v[1];v1=v[2];v2=v[3];
		}else return null;
	}
	return vec3NormalLine(v1 ,v2, v0);
}
*/
public struct polygon{
	public int[3] v;
	public Vector3 normal;
}

private Vector3[] multiple(Vector3[] a,float m){
	Vector3[] result;
	result.length = a.length;
	for (int i = 0;i < a.length;i ++){
		result[i] = a[i] * m;
	}
	return result;
}
private Vector3[] scale(Vector3[] a,float sx ,float sy ,float sz){
	Vector3[] result;
	result.length = a.length;
	for (int i = 0;i < a.length;i ++){
		result[i] = new Vector3(a[i].x * sx ,a[i].y * sy ,a[i].z * sz);
	}
	return result;
}
public class Shape:Cloneable{
	
	private Vector3[] fixedV;
	public Vector3[] v;
	public int[][] wires;
	public polygon[] polygons;
//	public Vector3[] normal;
	
	public this(){
		
	}
	public this(Vector3[] v ,int[][] wires, polygon[] polygons){
		set(v ,wires ,polygons);
	}
	public Shape multi(float mul){
		v = multiple(v ,mul);
		return this;
	}
	public Shape scalef(float sx ,float sy ,float sz){
		this.v = scale(v ,sx ,sy ,sz);
		return this;
	}
	public void set(Vector3[] v ,int[][] wires, polygon[] polygons){
		//this.fixedV = v.dup;
		this.fixedV.length = v.length;
		this.v.length = fixedV.length;
		for(int i = fixedV.length - 1;0 <= i;i --){
			this.fixedV[i] = new Vector3(v[i]);
			this.v[i] = new Vector3(fixedV[i]);
		}
		this.wires.length = wires.length;
		for(int i = wires.length - 1;0 <= i;i --){
			this.wires[i] = wires[i].dup;
		}
		this.polygons.length = polygons.length;
		for(int i = polygons.length - 1;0 <= i;i --){
			for(int j = polygons[i].v.length - 1;0 <= j;j --){
				this.polygons[i].v[j] = polygons[i].v[j];
			}
			if(polygons[i].normal !is null)
			 if(polygons[i].normal !is null)this.polygons[i].normal = new Vector3(polygons[i].normal);
			 else this.polygons[i].normal = new Vector3(1,0,0);
			else this.polygons[i].normal = new Vector3(1,0,0);
		}
		
	}
	
	public Shape opAdd(Shape s){
		
		Vector3[] v1;
		int[][] wires1;
		polygon[] polygons1;
		
		v1 = v ~ s.v;
		wires1 = wires;
		polygons1 = polygons;
		
		for(int i=0;i<s.wires.length;i++){
			int[] w;
			for(int j=0;j<s.wires[i].length;j++){
				w ~= s.wires[i][j] + v.length;
			}
			wires1 ~= w;
		}
		
		
		for(int i=0;i<s.polygons.length;i++){
			polygon p;
			p.v[0] = s.polygons[i].v[0] + v.length;
			p.v[1] = s.polygons[i].v[1] + v.length;
			p.v[2] = s.polygons[i].v[2] + v.length;
			p.normal = new Vector3(s.polygons[i].normal);
			
			polygons1 ~= p;
		}
		
		return new Shape(v1,wires1,polygons1);
	}
	
	public Object clone(){
    Shape shape = new Shape(v ,wires ,polygons);
    
    return shape;
  }
	public void resetVertex(){
		for(int i = v.length - 1;0 <= i;i --){
			v[i] = new Vector3(fixedV[i]);
		}
	}
	public Shape reverseX(){
		foreach(inout vec;v){
			vec.x = -vec.x;
		}
		return this;
	}
	public Shape reverseY(){
		foreach(inout vec;v){
			vec.y = -vec.y;
		}
		return this;
	}
	public Shape reverseZ(){
		foreach(inout vec;v){
			vec.z = -vec.z;
		}
		return this;
	}
	public Shape rotated(double rad ,double x,double y ,double z){
		
		
		Vector3 u = vec3Normalize(new Vector3(x ,y ,z));
		double sin = sin(rad);
		double cos = cos(rad);
		double[3][3] UU;
		UU[0][0] = u.x * u.x;UU[0][1] = u.x * u.y;UU[0][2] = u.x * u.z;
		UU[1][0] = u.y * u.x;UU[1][1] = u.y * u.y;UU[1][2] = u.y * u.z;
		UU[2][0] = u.z * u.x;UU[2][1] = u.z * u.y;UU[2][2] = u.z * u.z;
		double[3][3] T ;
		T[0][0] = cos + (1 - cos) * UU[0][0]; T[0][1] = (1 - cos) * UU[0][1] - u.z * sin; T[0][2] = (1 - cos) * UU[0][2] + u.y * sin;
		T[1][0] = (1 - cos) * UU[1][0] + u.z * sin; T[1][1] = cos + (1 - cos) * UU[1][1]; T[1][2] = (1 - cos) * UU[1][2] - u.x * sin;
		T[2][0] = (1 - cos) * UU[2][0] - u.y * sin; T[2][1] = (1 - cos) * UU[2][1] + u.x * sin; T[2][2] = cos + (1 - cos) * UU[2][2];
		for(int i = v.length - 1;0 <= i;i --){
			v[i] = new Vector3(
				T[0][0] * v[i].x + T[0][1] * v[i].y + T[0][2] * v[i].z ,
				T[1][0] * v[i].x + T[1][1] * v[i].y + T[1][2] * v[i].z ,
				T[2][0] * v[i].x + T[2][1] * v[i].y + T[2][2] * v[i].z
				);
		}
		return this;
	}
	
	public Shape translate(float x,float y,float z){
		Vector3 dvec = new Vector3(x,y,z);
		for(int i = fixedV.length - 1;0 <= i;i --){
			fixedV[i] += dvec;
		}
		resetVertex();
		return this;
	}
	
	
	public Shape transformMatrix(Matrix t){
		for(int i = v.length - 1;0 <= i;i --){
			v[i] = vec3translate(v[i] , t);
		}
		return this;
	}
	/*
	public void rollX(float d) {
		
		float sin = sin(d);
		float cos = cos(d);

		
		for(int i = v.length - 1;0 <= i;i --){
			float ty = v[i].y * cos - v[i].z * sin;
    	v[i].z = v[i].y * sin + v[i].z * cos;
    	v[i].y = ty;
		}

  }

  public void rollY(float d) {
		

		float sin = sin(d);
		float cos = cos(d);

		for(int i = v.length - 1;0 <= i;i --){
			float tx = v[i].x * cos - v[i].z * sin;
    	v[i].z = v[i].x * sin + v[i].z * cos;
    	v[i].x = tx;
		}
 
  }

  public void rollZ(float d) {
		float sin = sin(d);
		float cos = cos(d);

		for(int i = v.length - 1;0 <= i;i --){
    	float tx = v[i].x * cos - v[i].y * sin;
    	v[i].y = v[i].x * sin + v[i].y * cos;
    	v[i].x = tx;
		}
  }
	*/

}

public class Shape2D:Shape{
	public this(float[] a ,float[] b){
		if(a.length != b.length || a.length < 1){
			Log_write("Pole  arguments error" );
			throw new Exception("Pole  arguments error" );
		}
		Vector3[] v;
		int[][] wires;
		polygon[] polygons;
		for(int i=0;i<a.length;i++){
			v ~= new Vector3(a[i] ,b[i] ,0);
		}
		v ~= new Vector3(0 ,0 ,0);
		wires.length = 1;
		wires[0].length = a.length-1;
		polygons.length = a.length;
		for(int i=0;i<a.length-1;i++){
			wires[0][i] = i;
			polygons[i].v[0] = v.length-1;
			polygons[i].v[1] = i;
			polygons[i].v[2] = i+1;
			polygons[i].normal = new Vector3(0.0f ,0.0f ,1.0f);
		}
		wires[0] ~= a.length -1;
		wires[0] ~= 0;
		polygons[a.length-1].v[0] = v.length-1;
		polygons[a.length-1].v[1] = a.length-1;
		polygons[a.length-1].v[2] = 0;
		polygons[a.length-1].normal = new Vector3(0.0f ,0.0f ,1.0f);
		set(v ,wires ,polygons);
		
	}
}

public class SH_Pole:Shape{

	public this(float[] a ,float[] b ,int segment){
		if(a.length != b.length || a.length < 1){
			Log_write("Pole  arguments error" );
			throw new Exception("Pole  arguments error" );
		}
		Vector3[][] vec;
		vec.length = segment;
		for(int i = segment - 1;0 <= i;i --){
			vec[i].length = a.length;
		}
		for(int i = 0;i < segment ;i ++){
      for(int j = 0;j < a.length ;j ++){
				vec[i][j] = new Vector3();
        vec[i][j].x = a[j];
        vec[i][j].y = b[j] * cos(cast(float)i / cast(float)segment * PI * 2.0f);
        vec[i][j].z = b[j] * sin(cast(float)i / cast(float)segment * PI * 2.0f);
      }
    }
		joint(vec);
		
	}
	
	public this(float[][] a ,float[][] b ,int segment){
		
		// make wire frame
		
		if(a.length != b.length || a.length < 1){
			Log_write("Pole  arguments error" );
			throw new Exception("Pole  arguments error" );
		}
		int maxNum = 0;
		 for(int i = a.length -1;0 <= i;i --){
      if(a[i].length != b[i].length || a[i].length < 1){
        Log_write("Pole  arguments error" );
				throw new Exception("Pole  arguments error" );
      }
      if(maxNum < a[i].length)maxNum = a[i].length;
    }
		
		Vector3[][] vec;
		vec.length = segment;
		for(int i = segment - 1;0 <= i;i --){
			vec[i].length = maxNum;
		}
		int k,l;
		k = 0;
		for(int i = 0;i < segment ;i ++){
      for(int j = 0;j < maxNum ;j ++){
				if(j < a[k].length)l = j;
        else l = a[k].length - 1;
				vec[i][j] = new Vector3();
        vec[i][j].x = a[k][l];
        vec[i][j].y = b[k][l] * cos(cast(float)i / cast(float)segment * PI * 2.0f);
        vec[i][j].z = b[k][l] * sin(cast(float)i / cast(float)segment * PI * 2.0f);
      }
			k ++;
      if(a.length <= k)k = 0;
    }
		joint(vec);
		
	}
	public void joint(Vector3[][] vec){
		Vector3[] tv;
		polygon[] tpolygons;
		int[] twires;
		
		int number = vec[0].length;
		for(int i = vec.length -1;0 <= i;i --){
      if(vec[i].length != number || number < 1){
        Log_write("Pole joint arguments error" );
				throw new Exception("Pole joint arguments error" );
      }
    }
		tv.length = vec.length * vec[0].length;
		for(int i = 0;i < vec.length ;i ++){
      for(int j = 0;j < vec[i].length ;j ++){
				tv[i * vec[0].length + j] = vec[i][j];
      }
    }
		///*
		twires.length = (2 * vec.length + 1) * vec[0].length;
		int num = 0;
		 for(int j = 0;j < vec[0].length ;j ++){
      twires[num] = j;
      num ++;
      for(int i = 0;i < vec.length ;i ++){
				twires[num] = i * vec[0].length + j;
        num ++;
      }
      twires[num] = j;
      num ++;
    }
		///*
		int seg = 0;
    while(true){
      seg ++;
      if(vec.length <= seg)break; 
      for(int j = vec[0].length - 1;0 <= j ;j --){
				twires[num] = seg * vec[0].length + j;
        num ++;
      }
      seg ++;
      if(vec.length <= seg)break; 
      for(int j = 0;j < vec[0].length ;j ++){
				twires[num] = seg * vec[0].length + j;
        num ++;
      }
    }
		//make polygon
		///*
		tpolygons.length = vec.length  * (vec[0].length - 1) * 2;
		num = 0;
		for(int i = 0;i < vec[0].length -1 ;i ++){
			tpolygons[num].v[0] = (vec.length - 1) * vec[0].length + i;
			tpolygons[num].v[2] = (vec.length - 1) * vec[0].length + i + 1;
			tpolygons[num].v[1] = i;
			tpolygons[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);//vec3NormalLine(tv[tpolygons[num].v[0]] ,tv[tpolygons[num].v[1]] ,tv[tpolygons[num].v[3]]);
		//	polygons[num].normal = getNormal(polygons[num].v);//vec3NormalLine(polygons[num].v[0] ,polygons[num].v[2], polygons[num].v[3]);
			num ++;
			tpolygons[num].v[0] = i+1;
			tpolygons[num].v[1] = (vec.length - 1) * vec[0].length + i+1;
			tpolygons[num].v[2] = i;
			tpolygons[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
			num++;
		}
		///*
		for(int i = 0;i < vec.length - 1;i ++){
			for(int j = 0;j < vec[i].length - 1;j ++){
				tpolygons[num].v[0] = i * vec[0].length + j;
				tpolygons[num].v[2] = i * vec[0].length + j + 1;
				tpolygons[num].v[1] = (i + 1) * vec[0].length + j;
				tpolygons[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
			//	polygons[num].normal = getNormal(polygons[num].v);//vec3NormalLine(polygons[num].v[0] ,polygons[num].v[2], polygons[num].v[3]);
				num ++;
				tpolygons[num].v[0] = (i + 1) * vec[0].length + j+1;
				tpolygons[num].v[1] = i * vec[0].length + j+1;
				tpolygons[num].v[2] = (i+1) * vec[0].length + j;
				tpolygons[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
				num++;
			}
		}
		
		int[][] ttwires;
		ttwires.length = 1;
		ttwires[0] = twires.dup;
		set(tv ,ttwires ,tpolygons);
		
		
	
	}
	

}

public class SH_Pot:Shape{
	public this(float[] a ,float[] b ,float[] z ,float[] scale){
		
		Vector3[][] vec;
		if(a.length != b.length){
			Log_write("Pot arguments error");
			throw new Exception("Pot arguments error");
		}
		if(z.length != scale.length){
			Log_write("Pot arguments error");
			throw new Exception("Pot arguments error");
		}
		
		vec.length = z.length;
		for(int i = 0;i < vec.length; i ++){
			vec[i].length = a.length;
		}
		
		for(int i = 0;i < vec.length ;i ++){
			for(int j = 0;j < vec[i].length ;j ++){
				vec[i][j] = new Vector3(a[j] * scale[i] ,b[j] * scale[i] ,z[i]);
			}
		}
		
		joint(vec);
		
	}
	public void joint(Vector3[][] vec){
		
		Vector3[] tv;
		polygon[] tpolygons;
		int[][] twires;
		
		
		int number = vec[0].length;
		for(int i = vec.length -1;0 <= i;i --){
      if(vec[i].length != number || number < 1){
        Log_write("Pot joint arguments error" );
				throw new Exception("Pot joint arguments error" );
      }
    }
		
		tv.length = vec.length * vec[0].length;
		for(int i = 0;i < vec.length ;i ++){
	    for(int j = 0;j < vec[0].length ;j ++){
				tv[i * vec[0].length + j] = vec[i][j];
	    }
	  }
		twires.length = vec.length + vec[0].length;
		for(int i = 0;i < vec.length;i ++){
			twires[i].length = vec[i].length + 1;
		}
		for(int i = vec.length;i < twires.length;i ++){
			twires[i].length = vec.length;
		}
		for(int i = 0;i < vec.length;i ++){
			for(int j = 0;j < vec[i].length;j ++){
				twires[i][j] = i * vec[0].length + j;
			}
			twires[i][vec[i].length] = i * vec[0].length + 0;
		}
		for(int j = 0;j < vec[0].length;j ++){
			for(int i = 0;i < vec.length;i ++){
				twires[vec.length + j][i] = i * vec[0].length + j;
			}
		}
		int num = 0;
		/*
		//
		twires.length = ((vec[0].length + 1) * vec.length) + vec.length + vec.length * (vec[0].length - 1) + (vec[0].length - 1);
		int num = 0;
		
		for(int i = 0;i < vec.length ;i ++){
  	  twires[num] = i * vec[0].length + 0;
  	  num ++;
  	  for(int j = 0;j < vec[0].length ;j ++){
				twires[num] = i * vec[0].length + j;
  	    num ++;
  	  }
  	  twires[num] = i * vec[0].length + 0;
  	  num ++;
  	}
		
		int k ,l;
		l = 1;
		
		while(true){
			if(vec[0].length <= l)break;
			for(k = vec.length - 1;0 <= k;k --){
				twires[num] = k * vec[0].length + l;
				num ++;
			}
			l ++;
			if(vec[0].length <= l)break;
			for(k = 0;k < vec.length;k ++){
				twires[num] = k * vec[0].length + l;
				num ++;
			}
			l ++;
		}
		*/
		//
		tpolygons.length = (vec.length - 1) * vec[0].length*2;
		num = 0;
		for(int i = 0;i < vec.length - 1;i ++){
			for(int j = 0;j < vec[0].length - 1;j ++){
				tpolygons[num].v[0] = i * vec[0].length + j;
				tpolygons[num].v[1] = (i + 1) * vec[0].length + j;
				tpolygons[num].v[2] = i * vec[0].length + j + 1;
//				tpolygons[num].v[3] = i * vec[0].length + j + 1;
				tpolygons[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
				num ++;
				tpolygons[num].v[0] = (i + 1) * vec[0].length + j + 1;
				tpolygons[num].v[2] = (i + 1) * vec[0].length + j;
				tpolygons[num].v[1] = i * vec[0].length + j + 1;
				tpolygons[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
				num ++;
			}
		}
		for(int i = 0;i < vec.length - 1;i ++){
			tpolygons[num].v[0] = i * vec[0].length + vec[0].length - 1;
			tpolygons[num].v[1] = (i + 1) * vec[0].length + vec[0].length - 1;
			tpolygons[num].v[2] = i * vec[0].length + 0;
			
			tpolygons[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
			num ++;
			tpolygons[num].v[0] = (i+1) * vec[0].length + 0;
			tpolygons[num].v[2] = (i + 1) * vec[0].length + vec[0].length - 1;
			tpolygons[num].v[1] = i * vec[0].length + 0;
			tpolygons[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
			num ++;
		}
		int length = vec.length;
		if((1e-6) <= vec[0][0].size()){
			polygon[] cover;
			tv.length = tv.length + vec[0].length;
			cover.length =	vec[0].length;
			length ++;
			for(int i = 0;i < vec[0].length;i ++){
				tv[tv.length - vec[0].length + i] = new Vector3(0 ,0 ,vec[0][0].z);
			}
			num = 0;
			for(int j = 0;j < vec[0].length - 1;j ++){
				cover[num].v[0] = (length - 1) * vec[0].length + j;
				cover[num].v[1] = 0 * vec[0].length + j;
				cover[num].v[2] = 0 * vec[0].length + j + 1;
				
				cover[num].normal = vec3NormalLine(tv[cover[num].v[0]],tv[cover[num].v[1]],tv[cover[num].v[2]]);
				num ++;
				/*
				cover[num].v[0] = 0 * vec[0].length + j + 1;
				cover[num].v[1] = (length - 1) * vec[0].length + j;
				cover[num].v[2] = 0 * vec[0].length + j;
				cover[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
				num ++;
				*/
			}
			cover[num].v[0] = (length - 1) * vec[0].length + vec[0].length - 1;
			cover[num].v[1] = 0 * vec[0].length + vec[0].length - 1;
			cover[num].v[2] = 0 * vec[0].length + 0;
			cover[num].normal =  vec3NormalLine(tv[cover[num].v[0]],tv[cover[num].v[1]],tv[cover[num].v[2]]);
			num ++;
			/*
			cover[num].v[0] = 0 * vec[0].length + 0;
			cover[num].v[1] = (length - 1) * vec[0].length + vec[0].length - 1;
			cover[num].v[2] = 0 * vec[0].length + vec[0].length - 1;
			cover[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
			num ++;
			*/
			tpolygons = tpolygons~cover;
		}
		if((1e-6) <= vec[vec.length - 1][0].size()){
			polygon[] cover;
			tv.length = tv.length + vec[0].length;
			cover.length =	vec[0].length;
			length ++;
			for(int i = 0;i < vec[0].length;i ++){
				tv[tv.length - vec[0].length + i] = new Vector3(0 ,0 ,vec[vec.length - 1][0].z);
			}
			num = 0;
			for(int j = 0;j < vec[0].length - 1;j ++){
				cover[num].v[0] = (length - 1) * vec[0].length + j;
				cover[num].v[2] = (vec.length - 1) * vec[0].length + j;
				cover[num].v[1] = (vec.length - 1) * vec[0].length + j + 1;
				cover[num].normal =  vec3NormalLine(tv[cover[num].v[0]],tv[cover[num].v[1]],tv[cover[num].v[2]]);
				num ++;
				/*
				cover[num].v[0] = (vec.length - 1) * vec[0].length + j + 1;
				cover[num].v[1] = (length - 1) * vec[0].length + j;
				cover[num].v[2] = (vec.length - 1) * vec[0].length + j;
				cover[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
				num ++;
				*/
			}
			cover[num].v[0] = (length - 1) * vec[0].length + vec[0].length - 1;
			cover[num].v[2] = (vec.length - 1) * vec[0].length + vec[0].length - 1;
			cover[num].v[1] = (vec.length - 1) * vec[0].length + 0;
			cover[num].normal =  vec3NormalLine(tv[cover[num].v[0]],tv[cover[num].v[1]],tv[cover[num].v[2]]);
			num ++;
			/*
			cover[num].v[0] = (vec.length - 1) * vec[0].length + 0;
			cover[num].v[1] = (length - 1) * vec[0].length + vec[0].length - 1;
			cover[num].v[2] = (vec.length - 1) * vec[0].length + vec[0].length - 1;
			cover[num].normal = vec3NormalLine(tv[tpolygons[num].v[0]],tv[tpolygons[num].v[1]],tv[tpolygons[num].v[2]]);
			num ++;
			*/
			tpolygons = tpolygons~cover;
		}
		
		int[][] ttwires;
		//ttwires.length = 1;
		//ttwires[0] = twires.dup;
		set(tv ,twires ,tpolygons);
	
	}
}
public class SH_Sphere:SH_Pole{
	public this(int segment ,float w = 1.0f,float h = 1.0f){
		float[] a ,b;
		a.length = segment;
		b.length = segment;
		for(int i = 0;i < segment;i ++){
			a[i] = -cos(cast(double)i / cast(double)(segment - 1) * PI) * w;
			b[i] = sin(cast(double)i / cast(double)(segment - 1) * PI) * h;
		}
		super(a ,b ,segment);
	}
}
/*
public class SH_Oval:SH_Pot{
	public this(double width ,double height ,int segment){
		float[] a ,b,z,scale;
		a.length = segment;
		b.length = segment;
		z.length = 1;
		scale.length = 1;
		z[0] = 0.0;
		scale[0] = 1.0;
		for(int i = 0;i < segment;i ++){
			a[i] = cos(cast(double)i / cast(double)(segment - 1) * 2.0 *PI) * width;
			b[i] = sin(cast(double)i / cast(double)(segment - 1) * 2.0 * PI) * height;
		}
		super(a ,b ,z ,scale);
	}
}
*/
public class SH_Regular:Shape2D{
	
	public this(int num ,float w = 1.0f,float h = 1.0f){
		float[] a ,b;
		a.length =num;b.length = num;
		for(int i=0;i<a.length;i++){
			a[i] = cos(cast(double)i/cast(double)a.length*PI*2.0) * w;
			b[i] = sin(cast(double)i/cast(double)a.length*PI*2.0) * h;
		}
		super(a ,b);
	}
}

public class SH_Star:Shape2D{
	
	public this(int num ,float d = 0.5f ,float w = 1.0f ,float h = 1.0f){
		float[] a ,b;
		a.length =num*2;b.length = num*2;
		for(int i=0;i<a.length;i+=2){
			a[i] = cos(cast(double)i/cast(double)a.length*PI*2.0) * w;
			b[i] = sin(cast(double)i/cast(double)a.length*PI*2.0) * h;
		}
		for(int i=1;i<a.length;i+=2){
			a[i] = cos(cast(double)i/cast(double)a.length*PI*2.0) * w * d;
			b[i] = sin(cast(double)i/cast(double)a.length*PI*2.0) * h * d;
		}
		super(a ,b);
	}
}