module util.timer;


private import std.math;
private import util.ascii;
//public float timerR = 1.0,timerG = 1.0 ,timerB = 1.0 ;

public void drawTimer(int timer,float x,float y ,float scale = 1.0){
	
	if(3600 * 100 <= timer)timer = 3600 * 100 -1;
	
	drawNumber((((timer / 60) / 60) / 10) % 10,x + 0 ,y ,scale);
	drawNumber(((timer / 60) / 60) % 10,x + 30*scale ,y ,scale);
	drawColon(x+50*scale ,y ,scale);
	drawNumber(((timer / 60) % 60) / 10,x + 70*scale ,y ,scale);
	drawNumber(((timer / 60) % 60) % 10,x + 100*scale ,y ,scale);
	drawColon(x+120*scale ,y ,scale);
	drawNumber(cast(int)ceil((timer % 60) / 0.6) / 10  ,x + 150*scale ,y ,scale);
	drawNumber(cast(int)ceil((timer % 60) / 0.6) % 10 ,x + 180*scale ,y ,scale);
}

